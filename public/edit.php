<?php ?>
<html>
<head>
    <title>Twitter</title>
    <link rel="stylesheet" href="./vendor/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" charset="UTF-8" href="./vendor/slick.min.css" />
    <link rel="stylesheet" type="text/css" href="./vendor/slick-theme.min.css" />
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <?php include ('html/header.php'); ?>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 col-md-3 mt-md-5 mt-3">
                <div class="edit__profile-header border rounded bg-white">
                    <div class="edit__profile-header-bg">
                        <div class="container">
                            <div class="edit-profile-header__avatar-container">
                                <div class="edit-profile-header__avatar">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container edit__user">
                        <span class="edit__fullname font-weight-bold">User Name</span>
                        <span class="edit__username">@username</span>
                    </div>
                </div>
            </div>

        <div class="col-12 col-md-9 mt-md-5 mt-3">
            <div class="col border border-light shadow-sm bg-white mb-3 p-3">
            <form>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="editFirstName">First Name</label>
                            <input type="text" class="form-control" id="editFirstName" placeholder="First Name">
                        </div>
                        <div class="col-md-6">
                            <label for="editLastName">Last Name</label>
                            <input type="text" class="form-control" id="editLastName" placeholder="Last Name">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="editEmail">Email address</label>
                    <input type="email" class="form-control" id="editEmail" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="editUserName">Username</label>
                    <input type="text" class="form-control" id="editUserName" aria-describedby="usernameHelp" placeholder="Username">
                    <small id="usernameHelp" class="form-text text-muted">Username must be at least three(3) characters long and must can contain one underscore and number</small>
                </div>
                <div class="form-group">
                    <label for="editBio">Biography</label>
                    <input type="text" class="form-control" id="editBio" aria-describedby="bioHelp" placeholder="Bio">
                    <small id="bioHelp" class="form-text text-muted">Your bio goes here in max 140 characters.</small>
                </div>
                <div class="form-group">
                    <label for="editDateOfBirth">Date of birth</label>
                    <input type="date" class="form-control" id="editDateOfBirth" value="2000-01-01">
                </div>
                <input type="button" class="edit__updatebtn" value=" Update ">
            </form>

        </div>
            <div class="col edit-form border border-light shadow-sm bg-white mb-3 p-4">
                <form>

                    <div class="form-group">
                        <label for="editCurrentPassword">Current Password</label>
                        <input type="password" class="form-control" id="editCurrentPassword" placeholder="Current Password">
                    </div>
                    <div class="form-group">
                        <label for="editPassword">Current Password</label>
                        <input type="password" class="form-control" id="editPassword" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="editConfirmPassword">Current Password</label>
                        <input type="password" class="form-control" id="editConfirmPassword" aria-describedby="passwordHelp" placeholder="Confirm Password">
                        <small id="passwordHelp" class="form-text text-muted">Password must be at least six(6) characters long and must contain one uppercase and number.</small>

                    </div>
                    <input type="button" class="edit__updatebtn" value=" Change password ">
                </form>

            </div>
        </div>
    </div>
</div>
</div>
<!-- Scripts -->
<script src="https://kit.fontawesome.com/ba027b69cd.js" crossorigin="anonymous"></script>
<script src="./vendor/jquery-3.4.1.slim.min.js"></script>
<script src="./vendor/popper.min.js"></script>
<script src="./vendor/bootstrap.min.js"></script>
<script type="text/javascript" src="./vendor/slick.min.js"></script>

<!--	<script src="js/index.js"></script>-->
</body>
</html>
