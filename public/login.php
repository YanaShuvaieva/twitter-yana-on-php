<?php ?>
<html>
<head>
    <meta charset = "UTF-8">
    <meta name = "viewport" content = "width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="https://kit.fontawesome.com/be4ae73e78.js" crossorigin="anonymous"></script>    
<title>Document</title>
</head>
<body>
<?php include ('header-login.php'); ?>
    <main class = "login">
        <div class = "login-container">
            <div class = "login-main">
                <p class = "login-main__title">Log in to Twitter</p>
                <input type = "text" class = "login-main__line" placeholder = "Email or Username">
                <input type = "password" class = "login-main__line" placeholder = "Password">
                <a href="../index.php" class="submit__link"><input type = "button" class = "login-main__btn" value = "SUBMIT"></a>
            </div>
            <div class = "login-footer">
                <p class = "login-footer__question">Need a twitter? </p>
                <a class = "login-footer__href"
                href = "signup.php">SIGN UP</a>
            </div>
        </div>
    </main>
</body>
</html>