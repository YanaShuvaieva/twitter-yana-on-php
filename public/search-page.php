<?php ?>
<html>
<head>
    <title>Twitter</title>
    <link rel="stylesheet" href="./vendor/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" charset="UTF-8" href="./vendor/slick.min.css" />
    <link rel="stylesheet" type="text/css" href="./vendor/slick-theme.min.css" />
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php include ('html/header.php'); ?>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 col-md-4 mt-md-5 mt-3">
                <div class="search__profile-header border rounded bg-white">
                    <div class="search__profile-header-bg">
                        <div class="container">
                            <div class="search-profile-header__avatar-container">
                                <div class="search-profile-header__avatar">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container edit__user">
                        <span class="edit__fullname font-weight-bold">User Name</span>
                        <span class="edit__username">@username</span>
                        <div class="user__how-much d-flex mt-2">
                            <div class="d-block text-center mr-3">
                                <span class="user__twt font-weight-bold">Tweets</span>
                                <br>
                                <span class="user__tff font-weight-bold">0</span>
                            </div>
                            <div class="d-block text-center mr-3">
                                <span class="user__twt font-weight-bold">Following</span>
                                <br>
                                <span class="user__tff font-weight-bold">2</span>
                            </div>
                            <div class="d-block text-center mr-3">
                                <span class="user__twt font-weight-bold">Followers</span>
                                <br>
                                <span class="user__tff font-weight-bold">1</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-8 mt-md-5 mt-3">
                <div class="search-result d-flex p-2 bg-white">
                    <div class="find__avatar mr-2"></div>
                    <div class="d-flex flex-column">
                        <span class="find__l-f-name font-weight-bold">Test Test</span>
                        <span class="find__username fix-some-pos font-weight-bold">@test</span>
                        <input type="button" class="follow-find-res font-weight-bold" value="FOLLOWING">
                    </div>
                </div>
                <div class="search-result d-flex p-2 bg-white">
                    <div class="find__avatar mr-2"></div>
                    <div class="d-flex flex-column">
                        <span class="find__l-f-name font-weight-bold">Rozhuk Anton</span>
                        <span class="find__username fix-some-pos font-weight-bold">@towawtf</span>
                        <input type="button" class="follow-find-res font-weight-bold" value="FOLLOWING">
                    </div>
                </div>
                <div class="search-result d-flex p-2 bg-white">
                    <div class="find__avatar mr-2"></div>
                    <div class="d-flex flex-column">
                        <span class="find__l-f-name font-weight-bold">Rozhuk Anton кукукукукукукукук</span>
                        <span class="find__username fix-some-pos font-weight-bold">@towawtf</span>
                        <input type="button" class="follow-find-res font-weight-bold" value="FOLLOWING">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Scripts -->
<script src="https://kit.fontawesome.com/ba027b69cd.js" crossorigin="anonymous"></script>
<script src="./vendor/jquery-3.4.1.slim.min.js"></script>
<script src="./vendor/popper.min.js"></script>
<script src="./vendor/bootstrap.min.js"></script>
<script type="text/javascript" src="./vendor/slick.min.js"></script>

<!--	<script src="js/index.js"></script>-->
</body>
</html>
