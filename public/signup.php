<?php ?>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <script src="https://kit.fontawesome.com/be4ae73e78.js" crossorigin="anonymous"></script>    
    <title>Document</title>
</head>
<body>
<?php include ('header-login.php'); ?>
    <main class="registration-page">
        <div class="registr-container">
            <div class="registr-bord">
                <p class="registr-bord__title">Create an account</p>
                <div class="registr-line registr-first">
                    <input type="text" class="reg-style reg-style-2 reg-style-3" placeholder="First Name">
                    <input type="text" class="reg-style reg-style-2 " placeholder="Last Name">
                </div>
                <div class="registr-line">
                    <input type="text" class="reg-style" placeholder="Email">
                </div>
                <div class="registr-line">
                    <input type="text" class="reg-style" placeholder="Username">
                    <br>
                    <p class="registr__text ">Username must be at least three(3) characters long and must can contain one underscore and number.</p>
                </div>
                <div class="registr-line">
                    <input type="text"  class="reg-style reg-date" onfocus="(this.type='date')" placeholder="Date of birth">
                </div>
                <div class="registr-line">
                    <input type="password" class="reg-style" placeholder="Password">
                    <br>
                    <p class="registr__text">Password must be at least six(6) characters long and must can contain one underscore and number.</p>
                </div>
                <div class="registr-line">
                    <input type="password" class="reg-style" placeholder="Confirm password">
                </div> 
                <div class="registr-submit">
                    <a href="../index.php" class="submit__link"><input type="button" value="SUBMIT" class="registr-submit__btn"></a>
                </div>
            </div>
            <div class="registr-footer ">
                <p class="registr-footer__question ">Already using twitter?</p>
                <a href="login.php" class="registr-footer__log-href">LOGIN</a>
            </div>
        </div>
    </main>
</body>
</html>