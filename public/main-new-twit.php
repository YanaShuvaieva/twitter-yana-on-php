<div class="col-xl-9 col-md-9 col-sm-12">
                        <div class="tweets__new-tweet border shadow-sm mb-3 p-3">
                            <form>
                                <div class="form-group">
                                    <textarea class="form-control" id="tweetsNewTweet" placeholder="What's happening?"></textarea>
                                </div>

                                <div class="d-flex justify-content-end">
                                    <button type="button" class="btn btn-primary tweets__new-tweet-btn"><i class="fas fa-feather-alt"></i> Tweet</button>
                                </div>
                            </form>
                        </div>
                    </div>