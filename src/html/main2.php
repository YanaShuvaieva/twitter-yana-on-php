<div class="container container-tweets-custom">
        <div class="col-xl-9 col-md-9 col-sm-12">
            <div class="row">
                <div class="tweet rounded border">
                    <div class="tweet__time">
                        38m ago
                    </div>
                    <div class="tweet__text-huge">
                        Twitter changes its layout, again. This one
                        is a huge tweet.
                        <a href="" class="wrapping-link">#NewTwitter</a>
                    </div>
                    <div class="tweet__toolbar align-items-center">
                        <i class="far fa-comment toolbar-icon" style="color: #ccd6dd"></i>
                        <div class="toolbar-number">
                            150
                        </div>

                        <i class="fas fa-retweet toolbar-icon" style="color: #ccd6dd"></i>
                        <div class="toolbar-number">
                            35
                        </div>

                        <i class="fas fa-star toolbar-icon" style="color: #ccd6dd"></i>
                        <div class="toolbar-number">
                            50
                        </div>

                        <i class="fas fa-ellipsis-h" style="color: #ccd6dd"></i>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="tweet-media rounded border">
                    <div class="tweet__time">
                        1h ago
                    </div>
                    <div class="tweet__text-huge">
                        This is a tweet with a nice <a href="">#picture</a> attached.
                        Feel free to insert your favorite image here.
                    </div>
                    <div class="tweet__photo"></div>
                    <div class="tweet__toolbar align-items-center">
                        <i class="far fa-comment toolbar-icon" style="color: #ccd6dd"></i>
                        <div class="toolbar-number">
                            319
                        </div>

                        <i class="fas fa-retweet toolbar-icon" style="color: #ccd6dd"></i>
                        <div class="toolbar-number">
                            23
                        </div>

                        <i class="fas fa-star toolbar-icon" style="color: #ccd6dd"></i>
                        <div class="toolbar-number">
                            89
                        </div>

                        <i class="fas fa-ellipsis-h" style="color: #ccd6dd"></i>
                        <div class="more-options ml-auto toolbar-number">
                            <a href=""> View more photos</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="tweet rounded border">
                    <div class="tweet__time"> 2d ago</div>
                    <div class="tweet__text">
                        And now comes a regular tweet! Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim. <a href="" class="wrapping-link"> #placeholdertexе </a>
                    </div>
                    <div class="tweet__toolbar align-items-center">
                        <i class="far fa-comment toolbar-icon" style="color: #ccd6dd"></i>
                        <div class="toolbar-number">
                            150
                        </div>

                        <i class="fas fa-retweet toolbar-icon" style="color: #ccd6dd"></i>
                        <div class="toolbar-number">
                            35
                        </div>

                        <i class="fas fa-star toolbar-icon" style="color: #ccd6dd"></i>
                        <div class="toolbar-number">
                            50
                        </div>

                        <i class="fas fa-ellipsis-h" style="color: #ccd6dd"></i>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="retweet rounded border">
                    <div class="tweet__retweet">
                        <div class="retweet__time">
                            <div class="retweet-icon rounded">
                                <i class="fas fa-retweet toolbar-icon" style="color: #ffffff"></i>
                            </div>
                            <div class="retweet__text">
                                retweeted on feb 13
                            </div>
                        </div>
                        <div class="retweet__author">
                            <div class="author-icon"></div>
                            <div class="author-name">
                                <a href="">Giulio Bordonaro</a> @GiulioBX · Feb 13
                            </div>
                        </div>
                    </div>
                    <div class="tweet__text">
                        I hope you find this #PSD <a href="">#template</a> useful. You can find the latest version
                        on <a href="" class="wrapping-link">http://j.mp/twitter2014gui</a> (if you have any suggestion please contact me)
                    </div>
                    <div class="tweet__toolbar align-items-center">
                        <i class="far fa-comment toolbar-icon" style="color: #ccd6dd"></i>
                        <div class="toolbar-number">
                            150
                        </div>

                        <i class="fas fa-retweet toolbar-icon" style="color: #ccd6dd"></i>
                        <div class="toolbar-number">
                            35
                        </div>

                        <i class="fas fa-star toolbar-icon" style="color: #ccd6dd"></i>
                        <div class="toolbar-number">
                            50
                        </div>

                        <i class="fas fa-ellipsis-h" style="color: #ccd6dd"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="following__body-container main-tab-content" data-active="false">
    <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <div class="following__user-card border rounded">
                        <div class="following__background">
                            <div class="container">
                                <div class="following__avatar-container">
                                    <div class="following__avatar">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="button" class="following__followbtn" value="Follow">
                        <div class="container following__user">
                            <span class="following__fullname font-weight-bold">User Name</span>
                            <span class="following__username">@username</span>
                            <p class="following__biography">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                        </div>

                    </div>
                </div>
                <div class="col-12 col-sm-6">
                    <div class="following__user-card border rounded">
                        <div class="following__background">
                            <div class="container">
                                <div class="following__avatar-container">
                                    <div class="following__avatar">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="button" class="following__followbtn" value="Follow">
                        <div class="container following__user">
                            <span class="following__fullname font-weight-bold">User Name</span>
                            <span class="following__username">@username</span>
                            <p class="following__biography">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                        </div>

                    </div>
                </div>
                <div class="col-12 col-sm-6">
                    <div class="following__user-card border rounded">
                        <div class="following__background">
                            <div class="container">
                                <div class="following__avatar-container">
                                    <div class="following__avatar">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="button" class="following__followbtn" value="Follow">
                        <div class="container following__user">
                            <span class="following__fullname font-weight-bold">User Name</span>
                            <span class="following__username">@username</span>
                            <p class="following__biography">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                        </div>

                    </div>
                </div>
                <div class="col-12 col-sm-6">
                    <div class="following__user-card border rounded">
                        <div class="following__background">
                            <div class="container">
                                <div class="following__avatar-container">
                                    <div class="following__avatar">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="button" class="following__followbtn" value="Follow">
                        <div class="container following__user">
                            <span class="following__fullname font-weight-bold">User Name</span>
                            <span class="following__username">@username</span>
                            <p class="following__biography">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                        </div>

                    </div>
                </div>
            </div>

    </div>
</div>