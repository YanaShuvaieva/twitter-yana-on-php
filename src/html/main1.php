<div class="profile-background">
		<div class="container">
			<!-- User main avatar -->
			<div class="avatar-container">
				<div class="avatar">

				</div>
			</div>
		</div>
	</div>
		<div class="container ">
			<div class="row">
				<div class="col-12 col-md-3">
				<div class="profile-header">
    <h3 class="profile-element profile-element__fullname">Your Name Here</h3>
    <a class="text-primary profile-element profile-element__username ">@YourHandleHere</a>
    <p class="text-wrap profile-element profile-element__biography">Your bio goes here in max 140 characters. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut lab.</p>
    <span class="profile-element profile-element__location"><i class="fas fa-map-marker-alt" style="color: #41abe1"></i> Your location</span>
    <span class="profile-element profile-element__website"><a href="#" class="text-primary"><i class="fas fa-link" style="color: #41abe1"> Primary link</i></a></span>
</div>

<nav class="navbar navbar-expand-sm rounded border border-primary profile-search-user">
    <input class="form-control profile-search-user__input" type="text" placeholder="Tweet to Your Name here">
</nav>

<div class="follow-panel bg-white rounded ">
    <span class="follow-panel__header">Who to follow
        <a href="#" class="follow-panel__link text-primary">Refresh</a>
        <a href="#" class="follow-panel__link text-primary">View all</a>
    </span>
        <ul class="list-group list-group-flush">
            <li class="list-group-item justify-content-space-between">
                <div class="row">
                    <div class="col-xl-3 col-md-12">
                    <img src="build/img/user1.png">
                    </div>
                    <div class="col-xl-9">
                    <span class="follow-panel__user-data text-wrap font-weight-bold">Giulio Bordonaro </span>
                        <span class="follow-panel__user-data text-secondary font-weight-normal">@GiulioBx</span>
                    <button type="button" class="btn btn-outline-primary follow-panel__button"><i class="fas fa-user-plus follow-panel__follow-icon"></i> Follow</button>
                    </div>
                </div>
            </li>
            <li class="list-group-item">
                <div class="row">
                    <div class="col-xl-3 col-md-12">
                        <img src="build/img/user2.png">
                    </div>
                    <div class="col-xl-9">
                        <span class="follow-panel__user-data text-wrap font-weight-bold">Interesting User </span>
                        <span class="follow-panel__user-data text-secondary font-weight-normal">@User</span>
                        <button type="button" class="btn btn-outline-primary follow-panel__button"><i class="fas fa-user-plus follow-panel__follow-icon"></i> Follow</button>
                    </div>
                </div>
            </li>
            <li class="list-group-item">
                <div class="row">
                    <div class="col-xl-3 col-md-12">
                        <img src="build/img/user3.png">
                    </div>
                    <div class="col-xl-9">
                        <span class="follow-panel__user-data text-wrap font-weight-bold">Other User </span>
                        <span class="follow-panel__user-data text-secondary font-weight-normal">@Other User</span>
                        <button type="button" class="btn btn-outline-primary follow-panel__button"><i class="fas fa-user-plus follow-panel__follow-icon"></i> Follow</button>
                    </div>
                </div>
            </li>
        </ul>
    <div class="container row">
        <div class="col-md-6"><a href="#" class="follow-panel__link text-primary">Popular accounts </a></div>
        <div class="col-md-6"><a href="#" class="follow-panel__link text-primary"> Find friends</a></div>


    </div>

</div>
<div class="hashtag-panel bg-white rounded ">
    <span class="hashtag__header">Worldwide Trends</span>
    <a class="text-primary"> Change</a>
    <ul class="hashtag-list">
        <li class="hashtag-list__hashtag text-primary">#NewTwitter</li>
        <li class="hashtag-list__hashtag text-primary">#GBX</li>
        <li class="hashtag-list__hashtag text-primary">#graphicdesign</li>
        <li class="hashtag-list__hashtag text-primary">#mockup</li>
        <li class="hashtag-list__hashtag text-primary">#layout</li>
        <li class="hashtag-list__hashtag text-primary">#PSD</li>
        <li class="hashtag-list__hashtag text-primary">#FreeTemplate</li>
        <li class="hashtag-list__hashtag text-primary">#Lorem</li>
        <li class="hashtag-list__hashtag text-primary">#ipsum</li>
        <li class="hashtag-list__hashtag text-primary">#amet</li>
    </ul>
</div>
				</div>
				<div class="col-12 col-md-9">
				<nav class="navbar navbar-expand navbar-light navbar-custom">
    <div class="tweets__header-container">
        <div class="container">
            <div class="row w-100 border-bottom tweets__header__first-row">

                <div class="tweets__header__carousel">
                    <div class="tweets__header__nav-item" data-active="true" data-tab_target_class="tweets__body-container">
                        <div class="tweets_header__title">
                            Tweet
                        </div>
                        <div class="tweets_header__number"  data-active="true">
                            781
                        </div>
                        <div class="underline" data-active="true"></div>
                    </div>
                    <div class="tweets__header__nav-item" data-active="false">
                        <div class="tweets_header__title">
                            Photos/Videos
                        </div>
                        <div class="tweets_header__number" data-active="false">
                            62
                        </div>
                        <div class="underline" data-active="false"></div>
                    </div>
                    <div class="tweets__header__nav-item" data-active="false" data-tab_target_class="following__body-container">
                        <div class="tweets_header__title">
                            Following
                        </div>
                        <div class="tweets_header__number" data-active="false">
                            164
                        </div>
                        <div class="underline" data-active="false"></div>
                    </div>
                    <div class="tweets__header__nav-item" data-active="false" data-tab_target_class="following__body-container">
                        <div class="tweets_header__title">
                            Followers
                        </div>
                        <div class="tweets_header__number" data-active="false">
                            277
                        </div>
                        <div class="underline" data-active="false"></div>
                    </div>
                    <div class="tweets__header__nav-item" data-active="false">
                        <div class="tweets_header__title">
                            Favorites
                        </div>
                        <div class="tweets_header__number"  data-active="false">
                            24
                        </div>
                        <div class="underline" data-active="false"></div>
                    </div>
                    <div class="tweets__header__nav-item" data-active="false">
                        <div class="tweets_header__title">
                            View
                        </div>
                        <div class="tweets_header__number"  data-active="false">
                            Lists
                        </div>
                        <div class="underline" data-active="false"></div>
                    </div>
                </div>

                <div class="col-auto ml-auto following-area align-items-center justify-content-end">
                    <button type="button" class="btn btn-primary btn-sm following-button">Following</button>
                    <div class="settings" id="settingsDropdown" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                        <i class="fas fa-cog settings__gear-icon" style="color: #66757f"></i>
                        <i class="fas fa-chevron-down settings__arrow-icon" style="color: #66757f"></i>
                    </div>
                    <div class="dropdown-menu settings-menu" aria-labelledby="settingsDropdown">
                        <a class="dropdown-item" href="../edit.php">Edit</a>
                        <a class="dropdown-item" href="../main.php">Log Out</a>
                    </div>
                </div>
            </div>
            <div class="row mr-auto border-bottom w-100 tweets__header__second-row align-items-center">
                <div class="col-auto tweets__header__nav-item-second-row" data-active="true"> Tweets </div>
                <div class="col-auto tweets__header__nav-item-second-row" data-active="false"> Tweets and replies </div>
            </div>
        </div>
    </div>
</nav>


<div class="tweets__body-container main-tab-content" data-active="true">
    